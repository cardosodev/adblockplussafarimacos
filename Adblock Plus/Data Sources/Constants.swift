/*
 * This file is part of Adblock Plus <https://adblockplus.org/>,
 * Copyright (C) 2006-present eyeo GmbH
 *
 * Adblock Plus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Adblock Plus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
 */

import Foundation

struct Constants {
    // App Bundle Identifiers
    static let appIdentifier = "org.adblockplus.adblockplussafarimac"
    static let contentBlockerIdentifier = "org.adblockplus.adblockplussafarimac.AdblockPlusSafariContentBlocker"
    static let safariToolbarIdentifier = "org.adblockplus.adblockplussafarimac.AdblockPlusSafariToolbar"
    static let groupIdentifier = "GRYYZR985A.org.adblockplus.adblockplussafarimac"

    // Easylist Filename Variants
    static let easylist = "easylist_content_blocker"
    static let easylistExceptionRules = "easylist+exceptionrules_content_blocker"

    // User Defaults Keys
    /// Returns a Bool if app has previously run
    static let hasRun = "hasRun"
    /// Returns a string of the stored app version, set when app last run.
    static let storedAppVersion = "installedAppVersion"
    /// Returns a Bool to see if onboarding should be shown
    static let showOnboarding = "showOnboarding"
    /// Returns a Bool to see if Acceptable Ads is enabled
    static let acceptableAdsEnabled = "acceptableAdsEnabled"
    /// Returns an Array(String) of domains to be whitelisted
    static let whitelistArray = "whitelistArray"

    // URLs
    static let acceptableAdsReadMoreURL = "https://adblockplus.org/en/acceptable-ads#criteria"
    static let bugReportURL = "https://adblockplus.org/redirect?link=adblock_plus_report_bug"
    static let forumURL = "https://adblockplus.org/redirect?link=safari_support"
    static let supportEmailURL = "mailto:support@adblockplus.org"
    static let facebookURL = "https://adblockplus.org/redirect?link=social_facebook"
    static let twitterURL = "https://adblockplus.org/redirect?link=social_twitter"
    static let googlePlusURL = "https://adblockplus.org/redirect?link=social_gplus"

    // Storyboard Identifiers
    static let OnboardingVC = "OnboardingViewController"

    // NSCollectionViewItems
    static let sidebarItem = "SidebarItem"
    static let whitelistItem = "WhitelistItem"

    // App Version Number
    /// Returns a string of the version of the App from CFBundleShortVersionString in info.plist
    static var currentVersionNumber: String? {
        return Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
    }
}
