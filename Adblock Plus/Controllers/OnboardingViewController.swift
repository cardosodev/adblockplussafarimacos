/*
 * This file is part of Adblock Plus <https://adblockplus.org/>,
 * Copyright (C) 2006-present eyeo GmbH
 *
 * Adblock Plus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Adblock Plus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
 */

import Cocoa
import SafariServices

class OnboardingViewController: NSViewController {

    private var contentBlockerIsEnabled = false

    //Outlets for first screen
    @IBOutlet weak var screenOne: NSView!
    @IBOutlet weak var launchPreferencesButton: NSButton!
    @IBOutlet weak var screenOneHeaderLabel: NSTextField!
    @IBOutlet weak var screenOneInstructionsLabel: NSTextField!
    @IBOutlet weak var screenTwoHeaderLabel: NSTextField!

    //Outlets for second screen
    @IBOutlet weak var screenTwo: NSView!
    @IBOutlet weak var firstContentFrame: NSView!
    @IBOutlet weak var secondContentFrame: NSView!
    @IBOutlet weak var finishButton: NSButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        SFContentBlockerManager.getStateOfContentBlocker(withIdentifier: Constants.contentBlockerIdentifier) { state, _ in
            self.contentBlockerIsEnabled = state?.isEnabled ?? false
            self.setUpLayers()
            self.applyAttributedStrings()
        }
    }

    func applyAttributedStrings() {
        let screenOneHeaderString = NSMutableAttributedString()
            .normal("You're ")
            .bold("one step away ")
            .normal("from surfing the web without annoying ads!")
        screenOneHeaderLabel.attributedStringValue = screenOneHeaderString

        let screenTwoHeaderString = NSMutableAttributedString()
            .bold("All done! ")
            .normal("Adblock Plus is now ready to block ads.")
        screenTwoHeaderLabel.attributedStringValue = screenTwoHeaderString

        switch contentBlockerIsEnabled {
        case true:
            let userInstructionsString = NSMutableAttributedString()
                .normal("Turn on ")
                .bold("ABP Control Panel ")
                .normal("in Safari Extension Preferences to enable it in your Safari toolbar.")
            screenOneInstructionsLabel.attributedStringValue = userInstructionsString
        case false:
            let userInstructionsString = NSMutableAttributedString()
                .normal("Please ")
                .bold("turn on Adblock Plus ")
                .normal("in Safari Extension Preferences.")
            screenOneInstructionsLabel.attributedStringValue = userInstructionsString
        }
    }

    func setUpLayers() {
        self.firstContentFrame.applyBorderStyle()
        self.secondContentFrame.applyBorderStyle()
    }

    @IBAction func didPressLaunchPreferencesButton(_ sender: Any) {
        SFSafariApplication.showPreferencesForExtension(withIdentifier: Constants.contentBlockerIdentifier) { _ in
            //open safari preferences for extensions
        }
        self.showSecondScreen()
    }

    func showSecondScreen() {
            self.screenOne.isHidden = true
            self.screenTwo.isHidden = false
    }

    @IBAction func didPressFinishButton(_ sender: Any) {
        dismissViewController(self)
    }
}

// Extends NSView to abstract application of CALayer styles.
extension NSView {

    /// Applies CALayer with grey border around caller.
    func applyBorderStyle() {
        let layer = CALayer()
        layer.cornerRadius = 4.0
        layer.borderWidth = 1.0
        layer.borderColor = NSColor(calibratedWhite: 0.0, alpha: 0.15).cgColor
        layer.masksToBounds = false
        layer.needsDisplayOnBoundsChange = true
        self.wantsLayer = true
        self.layer = layer
    }
}
