/*
 * This file is part of Adblock Plus <https://adblockplus.org/>,
 * Copyright (C) 2006-present eyeo GmbH
 *
 * Adblock Plus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Adblock Plus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
 */

import Cocoa
import SafariServices

class WhitelistViewController: NSViewController {

    private let notification = Notification.Name(rawValue: "\(Constants.safariToolbarIdentifier).whitelist")
    private let whitelistItemDataSource = WhitelistItemDataSource()

    @IBOutlet weak var urlTextField: NSTextField!
    @IBOutlet weak var addWebsiteButton: NSButton!
    @IBOutlet weak var whitelistCollectionView: NSCollectionView!

    @IBAction func removeFromWhitelistButton(_ sender: NSButton) {

        // Get URL from sender's cell.
        let indexPath = IndexPath(item: sender.tag, section: 0)
        let cell = whitelistCollectionView.item(at: indexPath)
        guard let url = cell?.textField?.stringValue else { return }

        // Remove URL from array
        let whitelistDataSource = WhitelistItemDataSource()
        whitelistDataSource.removeURLfromArray(url: url)
        let container = Constants.groupIdentifier
        let groupDefaults = UserDefaults(suiteName: container)

        // Get new array, and apply whitelist to blocklists and reload contentblocker and collection view.
        guard let whitelistArray = groupDefaults?.stringArray(forKey: Constants.whitelistArray) else { return }
        let whitelistManager = WhitelistManager()
        whitelistManager.applyWhitelist(whitelistArray)
        SFContentBlockerManager.reloadContentBlocker(withIdentifier: Constants.contentBlockerIdentifier) { _ in
        }
        whitelistCollectionView.reloadData()
    }

    @IBAction func urlTextFieldEntered(_ sender: NSTextField) {
        if sender.stringValue.isEmpty { return }
        // Disable UI Elements
        sender.isEnabled = false
        addWebsiteButton.isEnabled = false

        // Format hostname string
        guard let hostname = NSString(string: sender.stringValue).whitelistedHostname() else {
            sender.isEnabled = true
            addWebsiteButton.isEnabled = true
            return
        }

        // Add Hostname to WhitelistArray
        let whitelistItemDataSource = WhitelistItemDataSource()
        whitelistItemDataSource.appendToWhitelist(hostname: hostname)

        // Reload NSCollectionView
        whitelistCollectionView.reloadData()

        // Apply Whitelist
        let whitelistArray = whitelistItemDataSource.getWhitelistArray()
        let whitelistManager = WhitelistManager()
        whitelistManager.applyWhitelist(whitelistArray)

        // Reload Content Blocker
        SFContentBlockerManager.reloadContentBlocker(withIdentifier: Constants.contentBlockerIdentifier) { _ in
        }
        // Enable UI Elements
        sender.stringValue = ""
        sender.isEnabled = true
        addWebsiteButton.isEnabled = true
    }

    @IBAction func addWebsiteToWhitelist(_ sender: NSButton) {
        urlTextFieldEntered(urlTextField)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        DistributedNotificationCenter.default().addObserver(self,
                                                            selector: #selector(self.updateWhitelist),
                                                            name: notification,
                                                            object: Constants.safariToolbarIdentifier)
    }

    @objc
    func updateWhitelist() {
        whitelistCollectionView.reloadData()
    }
}

// MARK: - Extends ViewController to act as data source for the whitelist NSCollectionView
extension WhitelistViewController: NSCollectionViewDataSource {

    func collectionView(_ collectionView: NSCollectionView, numberOfItemsInSection section: Int) -> Int {
        let numberOfItems = whitelistItemDataSource.numberOfWhitelistItems()
        return numberOfItems
    }

    func collectionView(_ collectionView: NSCollectionView, itemForRepresentedObjectAt indexPath: IndexPath) -> NSCollectionViewItem {
        let whitelistItem = NSUserInterfaceItemIdentifier(rawValue: Constants.whitelistItem)

        //swiftlint:disable:next force_cast
        let item = collectionView.makeItem(withIdentifier: whitelistItem, for: indexPath) as! WhitelistItem
        item.textField?.stringValue = whitelistItemDataSource.whitelistItemForIndexPath(indexPath: indexPath)
        item.trashcanOutlet.tag = indexPath.item
        item.trashcanOutlet.isHidden = true

        return item
    }

}
