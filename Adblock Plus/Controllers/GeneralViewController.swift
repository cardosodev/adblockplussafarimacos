/*
 * This file is part of Adblock Plus <https://adblockplus.org/>,
 * Copyright (C) 2006-present eyeo GmbH
 *
 * Adblock Plus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Adblock Plus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
 */

import Cocoa
import SafariServices

class GeneralViewController: NSViewController {

    @IBOutlet weak var readMoreButton: NSButton!
    @IBOutlet weak var acceptableAdsCheckbox: NSButton!
    @IBOutlet weak var acceptableAdsSpinner: NSProgressIndicator!

    /// Opens URL when button tapped
    ///
    /// - Parameter button: Unused
    @IBAction func openAcceptableAdsReadMoreURL(_ button: NSButton) {
        if let url = URL(string: Constants.acceptableAdsReadMoreURL) {
            NSWorkspace.shared.open(url)
        }
    }

    /// When checkbox is toggled this saves the state in NSUserDefaults
    ///
    /// - Parameter button: The button that was interacted with.
    @IBAction func toggleAcceptableAds(_ button: NSButton) {
        let checkboxState = button.state.rawValue

        let container = Constants.groupIdentifier
        let groupDefaults = UserDefaults(suiteName: container)

        switch checkboxState {
        // State is disabled
        case 0:
            groupDefaults?.set(false, forKey: Constants.acceptableAdsEnabled)
        // State is enabled
        case 1:
            groupDefaults?.set(true, forKey: Constants.acceptableAdsEnabled)
        // State is mixed, however this should not occur due to option being disabled.
        // Should this error occur, reset to first run default.
        default:
            button.state = .on
            groupDefaults?.set(false, forKey: Constants.acceptableAdsEnabled)
        }
        reloadContentBlocker()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        styleReadMoreButton(button: readMoreButton)
        setAcceptableAdsCheckboxState(button: acceptableAdsCheckbox)
        // Added a short delay to reload content blocker from when app launches to ensure that
        // any updates (such as copying filter lists and applying whitelists don't clash.
        // I haven't had this problem during my testing however, but may need a little extra time
        // for slower machines. In future, this can be refactored into a more elegant solution.
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(2), execute: {
            self.reloadContentBlocker()
        })
    }

    /// When app is launched, this method checks NSUserDefaults and sets the checkbox as per user preference.
    ///
    /// - Parameter button: The button to update state of.
    private func setAcceptableAdsCheckboxState(button: NSButton) {
        let container = Constants.groupIdentifier
        let groupDefaults = UserDefaults(suiteName: container)

        guard let acceptableAdsEnabled = groupDefaults?.object(forKey: Constants.acceptableAdsEnabled) as? Bool else {
            groupDefaults?.set(true, forKey: Constants.acceptableAdsEnabled)
            button.state = .on
            return
        }
        switch acceptableAdsEnabled {
        case true:
            button.state = .on
        case false:
            button.state = .off
        }
    }

    /// Applies colour & font style to NSButton Text
    ///
    /// - Parameter button: The button to update style of.
    private func styleReadMoreButton(button: NSButton) {
        let readMoreString = button.title
        let readMoreAttributes: [NSAttributedStringKey: Any] = [
            NSAttributedStringKey.foregroundColor: NSColor.systemBlue,
            NSAttributedStringKey.font: NSFont.systemFont(ofSize: 8)
        ]
        button.attributedTitle = NSAttributedString(string: readMoreString, attributes: readMoreAttributes)
    }

    /// Changes enabled state of checkbox and adds animation spinner.
    /// This should be used while loading blocklists. Such as when acceptable ads button is toggled.
    /// Always called on main thread.
    ///
    /// - enabled: If passed in true, this will enable the UI element. False, will disable them.
    private func changeAcceptableAdsCheckboxState(enabled: Bool) {
        DispatchQueue.main.async {
            switch enabled {
            case true:
                self.acceptableAdsCheckbox.isEnabled = true
                self.acceptableAdsSpinner.stopAnimation(nil)
            case false:
                self.acceptableAdsCheckbox.isEnabled = false
                self.acceptableAdsSpinner.startAnimation(nil)
            }
        }
    }

    /// Checks if content blocker extension is enabled, and then calls upon it to reload.
    /// Also updates UI in this view controller to avoid use while processing.
    private func reloadContentBlocker() {
        SFContentBlockerManager.getStateOfContentBlocker(withIdentifier: Constants.contentBlockerIdentifier) { state, _ in
            let uwState = state?.isEnabled ?? false
            if uwState {
                self.changeAcceptableAdsCheckboxState(enabled: false)
                SFContentBlockerManager.reloadContentBlocker(withIdentifier: Constants.contentBlockerIdentifier, completionHandler: { _ in
                    self.changeAcceptableAdsCheckboxState(enabled: true)
                })
            }
        }
    }
}
