# Adblock Plus for Safari (macOS)

Adblock Plus is a content blocker for Safari which blocks adverts from the web.
You can [download Adblock Plus](https://itunes.apple.com/us/app/adblock-plus-for-safari/id1432731683) for free on the Mac App Store.

## Building

### Requirements

- [XCode 9.4.1](https://developer.apple.com/xcode/)
- [SwiftLint](https://github.com/realm/SwiftLint) (optional)

### Building in XCode

1. Open `Adblock Plus for Safari.xcodeproj` in XCode.
2. Build & Run the project.